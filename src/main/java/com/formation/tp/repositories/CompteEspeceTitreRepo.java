package com.formation.tp.repositories;

import com.formation.tp.entities.CompteEspeceTitreEntitie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteEspeceTitreRepo  extends JpaRepository<CompteEspeceTitreEntitie, String> {

}
