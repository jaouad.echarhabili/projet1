package com.formation.tp.repositories;

import com.formation.tp.entities.ComptePPEntitie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComptePPRepo extends JpaRepository<ComptePPEntitie, String> {


}
