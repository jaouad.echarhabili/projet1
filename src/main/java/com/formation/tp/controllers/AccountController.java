package com.formation.tp.controllers;


import com.formation.tp.dto.CompteDto;
import com.formation.tp.entities.ComptePPEntitie;
import com.formation.tp.services.CompteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public record AccountController (CompteService compteService) {

   // @GetMapping("")

   // public List<CompteDto> getAccount() {

        // List<ComptePPEntitie> pPAcount = compteService.getAccount();

     //   return compteService.getAccount();

    //}

    @GetMapping("/Accounts")
    public List<ComptePPEntitie> getAccounts() {
        return compteService.getAccounts();
    }
    @GetMapping("/get")
    public List<CompteDto> getComptePP(){
        return compteService.getComptePP();

    }

}