package com.formation.tp.services;


import com.formation.tp.dto.CompteDto;
import com.formation.tp.entities.CompteEspeceTitreEntitie;
import com.formation.tp.entities.ComptePPEntitie;
import com.formation.tp.mapper.ComptePPMapper;
import com.formation.tp.repositories.CompteEspeceTitreRepo;
import com.formation.tp.repositories.ComptePPRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
public record CompteService(ComptePPRepo comptePPRepo, CompteEspeceTitreRepo compteEspeceTitreRepo) {

    // public List<ComptePPEntitie> getAccount() {

    //    return comptePPRepo.findAll();
    //}
    //public List<CompteDto> getAccount(){
    //  List<ComptePPEntitie> comptePPEntitie = comptePPRepo.findAll();
    //  return ComptePPMapper.map(comptePPRepo.findAll());
    //}


    public List<CompteDto> getAccounts() {
        List<ComptePPEntitie> comptePPList = comptePPRepo.findAll();
        List<CompteEspeceTitreEntitie> compteEspeceTitreList = compteEspeceTitreRepo.findAll();
        List<CompteDto> dtos = new ArrayList<>();

        comptePPList.sort((item1, item2) -> {
            if (item2.getDateModification() != null && item1.getDateModification() != null) {
                return item1.getDateModification().compareTo(item2.getDateModification());
            }
            if (item2.getDateModification() == null && item1.getDateModification() == null) {
                return 0;
            } else if (item2.getDateModification() == null) {
                return 1;
            } else {
                return -1;
            }
        });

        for (ComptePPEntitie entitie : comptePPList) {
            List<CompteEspeceTitreEntitie> tmp = new ArrayList<>();
            for (CompteEspeceTitreEntitie item : compteEspeceTitreList) {
                if (entitie.getCode().equals(item.getCode())) {
                    tmp.add(item);
                }
            }
            dtos.add(toDto(entitie, tmp));
        }

        return filter(dtos);

    }

    private List<CompteDto> filter(List<CompteDto> dtos) {
        return dtos.stream().filter(compteDto -> compteDto.getAge() > 30 && compteDto.getNombreCompteEspece() > 1).toList();
    }

    private CompteDto toDto(ComptePPEntitie comptePPEntitie, List<CompteEspeceTitreEntitie> compteEspeceTitreEntitie) {
        CompteDto compteDto = new CompteDto();
        compteDto.setNom(comptePPEntitie.getNom());
        if (comptePPEntitie.getDateNaissance() != null)
            compteDto.setAge(LocalDate.now().getYear() - comptePPEntitie.getDateNaissance().getYear());

        if (comptePPEntitie.getPaysResidence().equals("MA"))
            compteDto.setPaysResidence("Maroc");
        else
            compteDto.setPaysResidence("Etranger");

        compteDto.setNombreCompteEspece(0);

        for (CompteEspeceTitreEntitie entitie : compteEspeceTitreEntitie) {
            if (entitie.getDevise().equals("EURO")) {
                compteDto.setNombreCompteEspece(compteDto.getNombreCompteEspece() + 1);
            }

        }
        //compteDto.setPaysResidence(UUID.randomUUID().toString());
        return compteDto;

    }


//    public List<CompteDto> createAccount(List<ComptePPEntitie> comptePPEntitie){
//        List<CompteDto> dto = new ArrayList<CompteDto>();
//        CompteDto dtoEntitie = new CompteDto();
//        List<CompteEspeceTitreEntitie> compteEspeceTitreEntities =  compteEspeceTitreRepo.findAll();
//
//        for(ComptePPEntitie entitie: comptePPEntitie ) {
//
//
//               // dtoEntitie.setPrenom(entitie.getPrenom());
//                dtoEntitie.setNom(entitie.getNom());
//
//            if (entitie.getPaysResidence()=="MA")
//                dtoEntitie.setPaysResidence("Maroc");
//            else
//                dtoEntitie.setPaysResidence("Etranger");
//
//            if (entitie.getDateNaissance()!= null)
//                    dtoEntitie.setAge(LocalDate.now().getYear() - entitie.getDateNaissance().getYear());
//
//            dto.add(dtoEntitie);
//
//        }
//
//        return dto;}

//    public List<CompteDto> getComptePP() {
//
//        List<ComptePPEntitie> comptePPEntitie = comptePPRepo
//                .findAll()
//                .stream()
//                .filter(c -> (c.getDateNaissance() != null && LocalDate.now().getYear() - c.getDateNaissance().getYear() > 30))
//                .toList();
//
//        return createAccount(comptePPEntitie);
//
//
//    }

    // public List<ComptePPEntitie> getAccounts(){
    //   return comptePPRepo.findAll()
    //         .stream()
    //         .filter( c-> (c.getDateNaissance() != null && LocalDate.now().getYear() - c.getDateNaissance().getYear() > 30)
    //       ).toList();
//}

}