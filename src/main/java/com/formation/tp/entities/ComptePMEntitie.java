package com.formation.tp.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(schema = "FORMATION", name = "COMPTE_PM")
@AllArgsConstructor
@NoArgsConstructor
public class ComptePM {
}
