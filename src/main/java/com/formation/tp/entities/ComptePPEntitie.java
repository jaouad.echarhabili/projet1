package com.formation.tp.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Table(schema = "FORMATION", name = "COMPTE_PP")
@AllArgsConstructor
@NoArgsConstructor

public class ComptePPEntitie {
    @Id
    @Column(name="CODE")
    private String code;

    @Column(name = "PRENOM")
    private String prenom;

    @Column(name = "NOM")
    private String nom;

    @Column(name = "DATE_NAISSANCE")
    private LocalDate dateNaissance;

    @Column(name = "DATE_MODIFICATION")
    private LocalDate dateModification;

    @Column(name = "PAYS_RESIDENCE")
    private String paysResidence;

}
