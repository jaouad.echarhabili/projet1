package com.formation.tp.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(schema = "FORMATION", name = "COMPTE_ESPECE_TITRE")
@AllArgsConstructor
@NoArgsConstructor

public class CompteEspeceTitreEntitie {
    @Id
    @Column(name="CODE")
    private String code;

    @Column(name="DEVISE")
    private String devise;

}
